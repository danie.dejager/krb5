From 343e4042abdec8697d2c30eb84f70bdbd8388302 Mon Sep 17 00:00:00 2001
From: Julien Rische <jrische@redhat.com>
Date: Thu, 5 May 2022 17:15:12 +0200
Subject: [PATCH] [downstream] Allow krad UDP/TCP localhost connection with FIPS

libkrad allows to establish connections only to UNIX socket in FIPS
mode, because MD5 digest is not considered safe enough to be used for
network communication. However, FreeRadius requires connection on TCP or
UDP ports.

This commit allows TCP or UDP connections in FIPS mode if destination is
localhost.

Resolves: rhbz#2082189
---
 src/lib/krad/remote.c | 35 +++++++++++++++++++++++++++++++++--
 1 file changed, 33 insertions(+), 2 deletions(-)

diff --git a/src/lib/krad/remote.c b/src/lib/krad/remote.c
index 929f1cef67..063f17a613 100644
--- a/src/lib/krad/remote.c
+++ b/src/lib/krad/remote.c
@@ -33,6 +33,7 @@
 
 #include <string.h>
 #include <unistd.h>
+#include <stdbool.h>
 
 #include <sys/un.h>
 
@@ -74,6 +75,35 @@ on_io(verto_ctx *ctx, verto_ev *ev);
 static void
 on_timeout(verto_ctx *ctx, verto_ev *ev);
 
+static in_addr_t get_in_addr(struct addrinfo *info)
+{ return ((struct sockaddr_in *)(info->ai_addr))->sin_addr.s_addr; }
+
+static struct in6_addr *get_in6_addr(struct addrinfo *info)
+{ return &(((struct sockaddr_in6 *)(info->ai_addr))->sin6_addr); }
+
+static bool is_inet_localhost(struct addrinfo *info)
+{
+    struct addrinfo *p;
+
+    for (p = info; p; p = p->ai_next) {
+        switch (p->ai_family) {
+            case AF_INET:
+                if (IN_LOOPBACKNET != (get_in_addr(p) & IN_CLASSA_NET
+                                                      >> IN_CLASSA_NSHIFT))
+                    return false;
+                break;
+            case AF_INET6:
+                if (!IN6_IS_ADDR_LOOPBACK(get_in6_addr(p)))
+                    return false;
+                break;
+            default:
+                return false;
+        }
+    }
+
+    return true;
+}
+
 /* Iterate over the set of outstanding packets. */
 static const krad_packet *
 iterator(request **out)
@@ -460,8 +490,9 @@ kr_remote_send(krad_remote *rr, krad_code code, krad_attrset *attrs,
                                      (krad_packet_iter_cb)iterator, &r, &tmp);
     if (retval != 0)
         goto error;
-    else if (tmp->is_fips && rr->info->ai_family != AF_LOCAL &&
-        rr->info->ai_family != AF_UNIX) {
+    else if (tmp->is_fips && rr->info->ai_family != AF_LOCAL
+                          && rr->info->ai_family != AF_UNIX
+                          && !is_inet_localhost(rr->info)) {
         /* This would expose cleartext passwords, so abort. */
         retval = ESOCKTNOSUPPORT;
         goto error;
-- 
2.41.0

